package base.services;

import base.data.AlbumDao;
import base.data.IAlbumDao;
import base.pojos.Album;

import java.util.List;

public class AlbumService {
    private IAlbumDao albumDao;

    public AlbumService() {

    }

    public AlbumService(IAlbumDao albumDao) {
        this.albumDao = albumDao;
    }

    public Album findAlbum (int id) {
        return this.albumDao.getById(id);
    }

    public List<Album> findAllAlbums() {
        return this.albumDao.getAll();
    }

    // Todo: validate incoming album first
    public int saveAlbum(Album a) {
        return this.albumDao.save(a);
    }

    public int updateAlbum(Album a) {
        return this.albumDao.update(a);
    }

    public void deleteAlbum(Album a) {
        this.albumDao.delete(a);
    }

}
