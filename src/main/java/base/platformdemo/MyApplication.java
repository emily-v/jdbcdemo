package base.platformdemo;

import base.data.AlbumDao;
import base.data.ArtistDao;
import base.data.IAlbumDao;
import base.services.AlbumService;
import base.util.ConnectionUtil;

import java.sql.SQLException;

public class MyApplication {
    ConnectionUtil connectionUtil;
    IAlbumDao albumDao;
    ArtistDao artistDao;
    AlbumService albumService;
    public void run(String... args) {

        try {
            init();

            //show the initial menu
        } catch(SQLException ex) {
            System.err.println("Error starting up. Closing application");
            //don't do this IRL
            System.exit(-1);
        }

    }

    private void init() throws SQLException {
        // create the application level dependencies here
        // this facilitates extension and maintainable code

        // Create a connection util for use in all dao so that all daos
        // are accessing the same database
        ConnectionUtil connectionUtil = new ConnectionUtil("jdbc:postgresql://mydb-inclass.cmpzxiqqdpxe.us-east-2.rds.amazonaws.com:5432/dbhello",
                "emily_vong", "mydb2018", new org.postgresql.Driver());
        // Create a new Album dao and inject the newly created connectionUtil
        // We reference the object throught the IAlbumDao interface so we
        // can create an instance of any type of IAlbumDao and the services
        // we still work
        albumDao = new AlbumDao(connectionUtil);
        artistDao = new ArtistDao(connectionUtil);


        // Create a new Album service to interface with the album dao
        // this abstracts away the album dao so we don't have to worry
        // about how we are getting the data.
        // Inject the newly created IAlbumDao
        albumService = new AlbumService(albumDao);
    }
}

