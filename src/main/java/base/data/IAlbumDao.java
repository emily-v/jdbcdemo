package base.data;

import base.pojos.Album;

import java.util.List;

public interface IAlbumDao {
    Album getById (int id);
    List<Album> getAll();
    int save (Album a);
    int update(Album a);
    void delete(Album a);

}
