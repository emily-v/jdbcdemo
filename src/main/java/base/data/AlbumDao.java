package base.data;

import base.pojos.Album;
import base.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// DAO - data access object. object for persisting data
public class AlbumDao implements IAlbumDao{
    ConnectionUtil connectionUtil;

    public AlbumDao() {

    }

    public AlbumDao(ConnectionUtil connectionUtil) {
        this.connectionUtil = connectionUtil;
    }

    @Override
    public Album getById(int id) {
        Connection connection = null;
        Album a = null;

        try {
            // open a connection to the database
            connection = this.connectionUtil.newConnection();

            // turn off autocommit
            connection.setAutoCommit(false);

            // SQL Statement is susceptible to SQL injection - don't use
//            String sql = "Select * from chinook.\"Album\" where \"AlbumId\" = " + id;
            // This is better
            String sql = "Select * from chinook.\"album\" where \"AlbumId\" = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id); // paramindex corresponds to the number of "?" being targeted in the sql string

            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                a = new Album();
                a.setAlbumId(rs.getInt("AlbumId"));
                a.setTitle(rs.getString("Title"));
                a.setArtistId(rs.getInt("ArtistId"));
            }

            ps.close();

            connection.commit();

            // turn on autocommit
            connection.setAutoCommit(true);

            return a;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return a;
    }

    @Override
    public List<Album> getAll() {
        Connection c = null;
        List<Album> albums = null;
        try {
            c = connectionUtil.newConnection();
            c.setAutoCommit(false);
            albums = new ArrayList<>(); // diamond syntax new with Java 8
            String sql = "select chinook.album.\"AlbumId\", chinook.album.\"Title\", chinook.album.\"ArtistId\" " +
                    "from chinook.album";
            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Album temp = new Album();
                temp.setAlbumId(rs.getInt("AlbumId"));
                temp.setTitle(rs.getString("Title"));
                temp.setArtistId(rs.getInt("ArtistId"));

                albums.add(temp);
            }

            ps.close();
            c.commit();
            c.setAutoCommit(true);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                c.rollback(); // if it catches exception, roll back to previous state
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return albums;
    }

    @Override
    public int save(Album a) {
        Connection c = null;

        try {
            c = connectionUtil.newConnection();
            c.setAutoCommit(false);
            String idSql = "Select max(\"AlbumId\") as id from chinook.album"; // creating new id because it is not serialized in table
            String insert = "Insert into chinook.album (\"AlbumId\", \"Title\", \"ArtistId\") values " +
                    "(?, ?, ?)";


            //get the next id
            //there is an easier way to do this
            //if the table we are inserting into has a serial id
            //then we only need an insert statement and we don't have
            //to do this.
            //the insert statement would look like
            // insert into chinook.album("Title", "ArtistId") values (x, y) returning "AlbumId";
            PreparedStatement ps = c.prepareStatement(idSql);
            ResultSet rs = ps.executeQuery();

            rs.next();

            int newId = rs.getInt("id") + 1; // need to do this because AlbumId is not serialized

            //insert with new Id
            ps = c.prepareStatement(insert);
            ps.setInt(1, newId);
            ps.setString(2, a.getTitle());
            ps.setInt(3, a.getArtistId());

            int rows = ps.executeUpdate(); // executeUpdate returns the number of rows affected

            c.commit();
            c.setAutoCommit(true);
            if(rows > 0) {
                return newId;
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return 0;
    }


    @Override
    public int update(Album a) {
        Connection c = null;

        try {
            c = connectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "Update chinook.album set \"Title\" = ?, \"ArtistId\" = ? where \"AlbumId\" = ?";

            PreparedStatement ps = c.prepareStatement(sql);

            ps.setString(1, a.getTitle());
            ps.setInt(2, a.getArtistId());
            ps.setInt(3, a.getAlbumId());

            int rows = ps.executeUpdate();

            c.commit();
            c.setAutoCommit(true);
            if(rows > 0) {
                return a.getAlbumId();
            } else {
                return 0;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }

    @Override
    public void delete(Album a) {

        Connection c = null;

        try {
            // **Album has dependency - track
            // **Track has dependencies - PlaylistTrack, InvoiceLine
            // MUST DELETE ALL DEPENDENCIES BEFORE ABLE TO DELETE ALBUM!
            c = connectionUtil.newConnection();
            c.setAutoCommit(false);


            String trackQuery = "select \"TrackId\" from chinook.track where \"AlbumId\" = ?";
            List<Integer> trackIds = new ArrayList<>();
            PreparedStatement ps = c.prepareStatement(trackQuery);
            ps.setInt(1, a.getAlbumId());

            // get album track ids
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                trackIds.add(rs.getInt("TrackId"));
            }

            // remove the tracks from the Playlists and InvoiceLine
            deleteTracksFromPlaylists(trackIds, c);
            deleteTracksFromInvoiceLines(trackIds, c);

            // delete the tracks
            deleteAlbumTracks(a.getAlbumId(), c);

            // delete the album
            String albumQuery = "Delete from chinook.album where \"AlbumId\" = ?";
            ps = c.prepareStatement(albumQuery);
            ps.setInt(1, a.getAlbumId());
            ps.executeUpdate();

            c.commit();
            c.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void deleteAlbumTracks(Integer albumId, Connection c) throws SQLException {
        String sql = "delete from chinook.track where \"AlbumId\" =?";

            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, albumId);
            ps.executeUpdate();
            ps.close();
        }

    private void deleteTracksFromPlaylists(List<Integer> ids, Connection c) throws SQLException {
        String sql = "delete from chinook.playlisttrack where \"TrackId\" =?";

        for(final Integer id : ids) {
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            ps.close();
        }
    }
    private void deleteTracksFromInvoiceLines(List<Integer> ids, Connection c) throws SQLException {
        String sql = "delete from chinook.invoiceline where \"TrackId\" =?";

        for(final Integer id : ids) {
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            ps.close();
        }
    }
}
