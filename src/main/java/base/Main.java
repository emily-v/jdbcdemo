package base;

import base.data.AlbumDao;
import base.data.IAlbumDao;
import base.pojos.Album;
import base.services.AlbumService;
import base.util.ConnectionUtil;

import java.sql.SQLException;


// -- ALTERNATIVE MAIN CLASS/METHOD TO USE WITH MYAPPLICATION.JAVA --
//import base.platformdemo.MyApplication;

//public class Main {
//    public static void main(String[] args) {
//
//        new MyApplication().run(args);
//
//    }
//}
// -- END ALTERNATE --


public class Main {
    public static void main(String[] args) throws SQLException {
        try {
            // create the application level dependencies here
            // this facilitates extension and maintainable code

            // Create a connection util for use in all dao so that all daos are accessing the same database
            ConnectionUtil connectionUtil = new ConnectionUtil("jdbc:postgresql://mydb-inclass.cmpzxiqqdpxe.us-east-2.rds.amazonaws.com:5432/dbhello",
                    "emily_vong", "mydb2018", new org.postgresql.Driver());

            // Create a new Album Dao and inject the newly created connectionUtil
            // We reference the object through the IAlbumDao interface in order
            // to create an instance of any type of IAlbumDao and the services will still work
            IAlbumDao albumDao = new AlbumDao(connectionUtil); // **Should be IAlbumDao or AlbumDao?**

            // Create a new Album Service to interface with the Album Dao
            // This abstracts away the Album Dao so we don't have to worry about how we're getting the data.
            // Inject the newly created AlbumDao/IAlbumDao**
            AlbumService albumService = new AlbumService(albumDao);

            // -- use the album service to return one album by Id --
//            int id = 27;
//            Album a = albumService.findAlbum(id);
//
//            if (a != null) {
//                System.out.println(a);
//            } else {
//                System.out.println("No albums with id " + id + " found.");
//            }

            // -- Retrieve all albums --
//            List<Album> albums = albumService.findAllAlbums();
//
//            if(albums == null || albums.size() == 0) {
//                System.out.println("There are no albums to browse");
//            } else {
//                for(final Album album : albums) {
//                    System.out.println(album);
//                }
//            }

            // -- save new album --
//            Album a = new Album();
//            a.setTitle("Appetite For Destruction");
//            a.setArtistId(88);
//
//            int id = albumService.saveAlbum(a);
//
//            if(id > 0) {
//                a.setAlbumId(id);
//                System.out.println("New album saved\n" + a);
//            } else {
//                System.err.println("Error trying to save\n" + a);
//            }

            // -- update album --
//            Album a = new Album();
//            a.setAlbumId(348);
//            a.setTitle("Appetite For Destruction updated again");
//            a.setArtistId(88);
//
//            int id = albumService.updateAlbum(a);
//
//            if(id > 0) {
//                a.setAlbumId(id);
//                System.out.println("Album updated:\n" + a);
//            } else {
//                System.err.println("Error trying to update\n" + a);
//            }

            // -- delete album --
            Album a = new Album();
            a.setAlbumId(22);

            albumService.deleteAlbum(a);

        } catch (SQLException e) {
            System.err.println("You have an error");
            throw e;
        }
    }
}
















